class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, unique: true
      t.string :password_hash, null: false
      t.boolean :activated, default: false

      t.timestamps
    end
    add_index :users, :email
  end
end
