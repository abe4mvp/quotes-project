class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.integer :user_id, presence: true
      t.string :token, presence: true

      t.timestamps
    end
    add_index :sessions, :user_id
    add_index :sessions, :token
  end
end
