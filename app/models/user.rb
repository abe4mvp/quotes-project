require 'bcrypt'
class User < ActiveRecord::Base
  attr_accessible :email, :password
  attr_reader :password
  validates :email, uniqueness: true # full messages
  validates :email, :password_hash, :activation_token, presence: true
  before_validation { |user| user.activation_token = Session.generate_token! } #:create_activation_token!

  has_many(
  :sessions,
  class_name: "Session",
  foreign_key: :user_id,
  primary_key: :id
  )

  def password=(password)
    @password = password
    self.password_hash =  BCrypt::Password.create(password)
  end

  def password_is?(password)
    BCrypt::Password.new(self.password_hash).is_password?(password)
  end

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)
    (user && user.password_is?(password)) ? user : nil
  end

  def create_activation_token!
    self.activation_token = Session.generate_token!
  end

  def activate!(query_token)
    if query_token == self.activation_token
      self.activated = true
      self.save
    else
      false
    end
  end


end
