class UsersController < ApplicationController

  def index
    @users = User.all #does this method make sense in long term?
    render :index
  end

  def new
    render :new
  end

  def create
    new_user = User.new(params[:user])

    if new_user.save
      msg = UserMailer.welcome_email(new_user)
      msg.deliver!
      flash[:cookies] = "Thanks for Signing up! Check your email to confirm registration!"
      session[:cookies] = {activated: true}
      redirect_to root_url
    else
      render text: new_user.errors
    end
  end

  def show
    @user = User.find(params[:id])
    render :show
  end

  def activate
    query_token = params[:token]
    user = User.find(params[:id])

    if user && query_token && user.activate!(query_token)
      sign_in_user!(user)
      redirect_to user
    else
      render text: user.errors #possibly query token error?
    end
  end
end
