class SessionsController < ApplicationController
  def new
    render :new
  end

  def create
    user = User.find_by_credentials(get_email, get_password)
    if !user
      flash[:cookies] = "Sorry, we have no record of your account, "
      redirect_to root_url
    elsif sign_in_user!(user) # redirects to home page if inactive account
      redirect_to user 
    else
      flash[:cookie] = "Credentials were wrong"
      redirect_to root_url
    end
  end
end
