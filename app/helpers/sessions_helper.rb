module SessionsHelper
  def sign_in_user!(user) #must test
    if user.activated == true
      self.current_user = user
      redirect_to user
    else
      flash[:cookies] = "Please check your email and activate your account before signing in"
      redirect_to root_url
    end
  end

  def current_user=(user)
    @current_user = user
    new_session = Session.create_for(user)
    session[:session_token] = new_session.token
  end

  def current_user
    current_token = session[:session_token]
    return unless secret_token
    session = Session.find_by_token(current_token)
    (@current_user ||= session.user) if session
  end

  def get_password
    params[:user][:password]
  end

  def get_email
    params[:user][:email]
  end

end
